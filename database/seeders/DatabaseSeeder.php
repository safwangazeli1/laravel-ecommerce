<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Category::create([
            'name'=>'laptop',
            'slug'=>'laptop',
            'description'=>'laptop category',
            'image'=>'files/photo1.jpeg'
        ]);
        Category::create([
            'name'=>'mobile phone',
            'slug'=> 'mobile-phone',
            'description'=>'mobile phone category',
            'image'=>'files/photo1.jpeg'
        ]);
        Category::create([
            'name'=>'Hearfone',
            'slug'=> 'haerfone',
            'description'=>'haerfone phone category',
            'image'=>'files/photo1.jpeg'
        ]);

        Subcategory::create([
            'name'=>'Dell',
            'category_id'=> '1',
        ]);

        Subcategory::create([
            'name'=>'HP',
            'category_id'=> '1',
        ]);

        Subcategory::create([
            'name'=>'Lenovo',
            'category_id'=> '1',
        ]);

        Product::create([
            'name'=>'HP Laptops',
            'price'=> rand(700,1000),
            'description'=>'This is the description product',
            'image'=>'product/2ZX7RZfBeAQxihd4QRslNIoAE6U1wDXIVBw6tF4a.png',
            'additional_info' => 'this is additional info',
            'category_id' => rand(1,3),
            'subcategory_id' => 1
        ]);

        Product::create([
            'name'=>'Dell Laptops',
            'price'=> rand(700,1000),
            'description'=>'This is the description product',
            'image'=>'product/5mEVRADhHgIEOOFmJ8TnQ6txsiZlakZwVlpc5VQT.png',
            'additional_info' => 'this is additional info',
            'category_id' => rand(1,3),
            'subcategory_id' => 2
        ]);

        Product::create([
            'name'=>'Lenovo laptop',
            'price'=> rand(700,1000),
            'description'=>'This is the description product',
            'image'=>'product/kcfA3ITsFWBhlShCZmQGpGB62tiKoodeWyFJGV94.jpg',
            'additional_info' => 'this is additional info',
            'category_id' => rand(1,3),
            'subcategory_id' => 3
        ]);

        User::create([
            'name'=>'Admin',
            'email'=>'admin@gmail.com',
            'password'=> bcrypt('password'),
            'email_verified_at'=>NOW(),
            'address'=>'Malaysia',
            'phone_number'=>'01901',
            'is_admin'=> 1,
        ]);

    }
}
