<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Models\User;
use App\Models\Order;

use App\Mail\Sendmail;
use App\Models\Product;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class CartController extends Controller
{
    public function addToCart(Product $product){
        if(session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        }else{
            $cart = new Cart();
        }

        $cart->add($product);
        // dd($cart);

        session()->put('cart', $cart);
        notify()->success('Product added to cart');
        return redirect()->back();
    }

    public function showCart(){
        if(session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        }else{
            $cart = null;
        }

        // dd($cart->items);
        
        return view('cart', compact('cart'));
    }

    public function updateCart(Request $request, Product $product){

        $request->validate([
            'qty'=> 'required|numeric|min:1'
        ]);

        $cart = new Cart(session()->get('cart'));
        $cart->updateQty($product->id, $request->qty);
        session()->put('cart', $cart);
        notify()->success('Cart Updated!');
        return redirect()->back();
    }

    public function removeCart(Product $product){
        $cart = new Cart(session()->get('cart'));
        $cart->remove($product->id);
        if($cart->totalQty<=0){
            session()->forget('cart');
        }else{
            session()->put('cart',$cart);
            notify()->success('Cart Updated!');
            return redirect()->back();
        }
    }

    public function checkout($amount){
        if(session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        }else{
            $cart = null;
        }  
        return view('checkout',compact('amount','cart'));
        
    }

    public function charge(Request $request){
        // return $request->stripeToken;
        // $stripe = new \Stripe\StripeClient('pk_test_51LMJj0GrIbONoClbWmPaBCjd6dp3k20TGuYX7V1CXwctleLPLVecAsAu27Fy3Ief5isMmyIKGCGn2TisR2kJjLYG00rmIsb3Ur');
        $charge = Stripe::charges()->create([
        // $charge = $stripe->paymentIntents->create([
            'currency'=>"USD",
            'source'=>$request->stripeToken,
            'amount'=>$request->amount,
            'description'=>'Test'
        ]);

        $chargeId = $charge['id'];
        if(session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        }else{
            $cart = null;
        } 
        \Mail::to(auth()->user()->email)->send(new Sendmail($cart));

      

        if($chargeId){
            auth()->user()->orders()->create([

                'cart'=>serialize(session()->get('cart'))
            ]);

            session()->forget('cart');
            notify()->success(' Transaction completed!');
            return redirect()->to('/');

        }else{
            return redirect()->back();
        }
        
    }

    public function order(){
        $orders = auth()->user()->orders;
    
        $carts =$orders->transform(function($cart,$key){
            return unserialize($cart->cart);

        });

        return view('order', compact('carts'));
    }

    public function userOrder(){
        $orders = Order::latest()->get();
        return view('admin.order.index', compact('orders'));
    }

    public function viewUserOrder($userid,$orderid) {
        $user = User::find($userid);
        $orders = $user->orders->where('id',$orderid);
    
        $carts =$orders->transform(function($cart,$key){
            return unserialize($cart->cart);

        });

        return view('admin.order.show', compact('carts'));
    }
}
