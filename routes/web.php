<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\FrontProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::resource('category','CategoryController');

// Route::get('/category', [ CategoryController::class, 'category'] );


Route::get('subcatories/{id}', [ProductController::class, 'loadSubcategories'] );



// Route::get('/index/test', function () {
//     return view('layouts/test');
// });

Route::get('/', [FrontProductController::class, 'index']);

Route::get('product/{id}', [FrontProductController::class, 'show'] )->name('product.view');

Route::get('/category/{name}',[FrontProductController::class, 'allProduct'])->name('product.list');

Auth::routes();

// // buang nanti 
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/addToCart/{product}', [CartController::class, 'addToCart'])->name('add.cart');

Route::get('/cart', [CartController::class, 'showCart'])->name('cart.show');

Route::post('/products/{product}', [CartController::class, 'updateCart'])->name('cart.update');

Route::post('/product/{product}', [CartController::class, 'removeCart'])->name('cart.remove');

Route::get('/checkout/{amount}', [CartController::class, 'checkout'])->name('cart.checkout')->middleware('auth');

Route::post('/charge', [CartController::class, 'charge'])->name('cart.charge');

Route::get('/orders', [CartController::class, 'order'])->name('order')->middleware('auth');

Route::get('all/products', [FrontProductController::class, 'moreProducts'])->name('more.product');

// sementara
// Route::get('slider', [SliderController::class, 'index'])->name('slider.index');

// Route::get('slider/create', [SliderController::class, 'create'])->name('slider.create');

// Route::post('slider', [SliderController::class, 'store'])->name('slider.store');


Route::group(['prefix'=>'auth','middleware'=>['auth','isAdmin']], 
    function(){
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');
    
    Route::resource('category', CategoryController::class);

    Route::resource('subcategory', SubcategoryController::class);

    Route::resource('product', ProductController::class);

    Route::get('slider', [SliderController::class, 'index'])->name('slider.index');

    Route::get('slider/create', [SliderController::class, 'create'])->name('slider.create');

    Route::post('slider', [SliderController::class, 'store'])->name('slider.store');
    
    Route::post('slider/{id}', [SliderController::class, 'destroy'])->name('slider.destroy');

    Route::get('users', [UserController::class, 'index'])->name('user.index');

    Route::get('/orders', [CartController::class, 'userOrder'])->name('order.index');

    Route::get('/orders/{user_id}/{order_id}', [CartController::class, 'viewUserOrder'])->name('user.order');

    // Route::delete('slider/{id}','SliderController@destroy')->name('slider.destroy');
    // Route::delete('slider/{id}', [SliderController::class, 'destroy'])->name('slider.destroy');



});


